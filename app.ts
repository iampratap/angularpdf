import { Component, OnInit } from "@angular/core";

import * as pdfjsLib from "pdfjs-dist/build/pdf";
pdfjsLib.GlobalWorkerOptions.workerSrc = " ./assets/pdf.worker.min.js";

@Component({
  selector: "app-application",
  template: `
  <input type="file" (change)="openfile($event)" accept="application/pdf">

<canvas id="the-canvas"></canvas>`
})
export class ApplicationComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  openfile(event) {
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onload = () => {
      var arrayBuffer = reader.result;
      // var password = prompt("Enter the password.");
      // console.log(password);
      const loadingTask = pdfjsLib.getDocument(arrayBuffer);

      loadingTask.onPassword = (updateCallback, reason) => {
        console.log(reason);
        if (reason === 1) {
          // need a password
          var new_password = prompt("Please enter a password:");
        } else {
          // Invalid password
          var new_password = prompt("Invalid! Please enter a password:");
        }
        updateCallback(new_password);
      };

      loadingTask.promise
        .then((pdf) => {
          console.log(pdf);
          pdf.getPage(1).then((page) => {
            var scale = 1.5;
            var viewport = page.getViewport({ scale: scale });
            var canvas = <HTMLCanvasElement>(
              document.getElementById("the-canvas")
            );
            console.log(canvas);
            var context = canvas.getContext("2d");
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            var renderContext = {
              canvasContext: context,
              viewport: viewport,
            };
            page.render(renderContext);
          });
        })
        .catch((err) => console.log(err));
    };
    reader.readAsArrayBuffer(file);
  }
}
